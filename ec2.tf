resource "aws_key_pair" "appsrv_key" {
    key_name   = "MyKeyPair"
    public_key = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQCl4sl5DSxASRHb4kgT7Z8RCJrinymE/ooRPaVdG2dTmiK2CTKeiSrdk0fmmz2XKC2m6HN+tyHP/GJXKhhJOZrkKeHnqdyyZXiKbKktJMkwT40UWJTCXJcAYs/4kTnOmy+cl2k13H9LKxw/BqgIKBAXl4rLZyC96XSt3vwEqSvF6ge8uZuMdTVNy9V1FXQnT3BajZXMISDy/VkR1MoMhN+AUkr0YKzSb4Y9qXU0yzFvZtzX0SL8rFxBCfvCnOamMa8UKehYGi4x7hSUgbwOQEfeP0YGxXugTYtFL72dYoK6iURUDZdmKdnOe1o7765iDElIl3RvCiOmaOc4qwqtm6CV"
}

resource "aws_instance" "appsrv" {
    ami               = "ami-07c8bc5c1ce9598c3"
    instance_type     = "t2.micro"
    key_name          = aws_key_pair.appsrv_key.key_name
    user_data         = file("files/alias.sh")

    subnet_id                 = aws_subnet.app_subnet01.id
    vpc_security_group_ids    = [aws_security_group.webservice_sg.id,aws_security_group.from_jumpbox_sg.id]
    
    root_block_device {
        volume_size           = 10
        volume_type           = "gp2"
        encrypted             = false
        delete_on_termination = true
    }
    
    volume_tags = {
        Name    = "${var.resrc_prefix}-appsrv01-vl"
    }

    tags = {
        Name = "${var.resrc_prefix}-appsrv01"
    }
}

resource "aws_ebs_volume" "appsrv_swapvl" {
    availability_zone = var.main_zone01
    type              = "gp2"
    size              = 2

    tags = {
        Name = "${var.resrc_prefix}-appsrv01-vl"
    }
}

resource "aws_volume_attachment" "appsrv_ebs_att_swapvl" {
    device_name = "/dev/xvdb"
    volume_id   = aws_ebs_volume.appsrv_swapvl.id
    instance_id = aws_instance.appsrv.id
}

resource "aws_instance" "jumpbox" {
    ami                         = "ami-0221711e02d773502"
    instance_type               = "t2.micro"
    key_name                    = aws_key_pair.appsrv_key.key_name
    source_dest_check           = false
    associate_public_ip_address = true
    // user_data         = file("files/jalias.sh")

    subnet_id                   = aws_subnet.pub_subnet01.id
    vpc_security_group_ids      = [aws_security_group.ssh_sg.id,aws_security_group.jumpbox_nat_sg.id]
    
    root_block_device {
        volume_size             = 10
        volume_type             = "gp2"
        encrypted               = false
        delete_on_termination   = true
    }

    provisioner "file" {
        source                  = "files/jalias.sh"
        destination             = "/tmp/srvalias.sh"

        connection {
            type                = "ssh"
            user                = "ec2-user"
            private_key         = file("~/Works/Keys/Infra/nals-infra-bk.pem")
            host                = self.public_ip
        }
    }

    /* provisioner "remote-exec" {
        inline = [
            "ls /tmp/srvalias.sh",
        ]
    } */
    
    volume_tags = {
        Name    = "${var.resrc_prefix}-jumpbox-vl"
    }

    tags = {
        Name = "${var.resrc_prefix}-jumpbox"
    }
}

resource "aws_ebs_volume" "jumpbox_swapvl" {
    availability_zone = var.main_zone01
    type              = "gp2"
    size              = 2

    tags = {
        Name = "${var.resrc_prefix}-jumpbox-vl"
    }
}

resource "aws_volume_attachment" "jumpbox_ebs_att_swapvl" {
    device_name = "/dev/xvdb"
    volume_id   = aws_ebs_volume.jumpbox_swapvl.id
    instance_id = aws_instance.jumpbox.id
}