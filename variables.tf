variable "resrc_prefix" {
    type    = string
    default = "asgard-stg"
}

variable "main_zones" {
    default =  ["us-east-2a","us-east-2b"]
}

variable "main_zone01" {
    description = "A main avaialbility zone"
    default = "us-east-2a"
}

variable "main_zone02" {
    description = "A main avaialbility zone"
    default = "us-east-2b"
}