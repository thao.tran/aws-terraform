resource "aws_db_subnet_group" "rds_subnet_group" {
	name       	= "${var.resrc_prefix}-rds-subnet-grp"
	description = "The subnet group of ${var.resrc_prefix} RDS"
	subnet_ids 	= [aws_subnet.db_subnet01.id, aws_subnet.db_subnet02.id]

	tags = {
		Name 	= "${var.resrc_prefix}-rds-subnet-grp"
	}
}

resource "aws_db_parameter_group" "rds_parameter_group" {
	name   		= "${var.resrc_prefix}-rds-parameter-grp"
	description = "The parameter group of ${var.resrc_prefix} RDS"
	family 		= "mysql5.7"

	tags = {
		Name   	= "${var.resrc_prefix}-rds-parameter-grp"
	}
}

resource "aws_db_option_group" "rds_option_group" {
	name   					 = "${var.resrc_prefix}-rds-option-grp"
	option_group_description = "The option group of ${var.resrc_prefix} RDS"
	engine_name 			 = "mysql"
	major_engine_version	 = "5.7"

	tags = {
		Name   				 = "${var.resrc_prefix}-rds-option-grp"
	}
}

resource "aws_db_instance" "rds" {
	allocated_storage       = "20"
	storage_type            = "gp2"
	engine                  = "mysql"
	engine_version          = "5.7.26"
	instance_class          = "db.t2.micro"
	identifier              = "${var.resrc_prefix}-db"
	username                = "admin"
	password                = "admin#1234"
	publicly_accessible     	= false
	multi_az					= false
	auto_minor_version_upgrade 	= false
	vpc_security_group_ids  	= ["${aws_security_group.rds_sg.id}"]
	parameter_group_name    	= aws_db_parameter_group.rds_parameter_group.name
	db_subnet_group_name    	= aws_db_subnet_group.rds_subnet_group.name
	availability_zone			= var.main_zone01
}

