resource "aws_vpc" "prj_vpc" {
    cidr_block           = "10.10.0.0/16"
    enable_dns_hostnames = true
    enable_dns_support   = true
    instance_tenancy     = "default"

    tags = {
        Name = "${var.resrc_prefix}-vpc"
    }
}

resource "aws_subnet" "pub_subnet01" {
    vpc_id     = aws_vpc.prj_vpc.id
    availability_zone = var.main_zone01
    cidr_block = "10.10.11.0/24"
    map_public_ip_on_launch = true

    tags = {
        Name = "${var.resrc_prefix}-pub-subnet01"
    }
}

resource "aws_subnet" "pub_subnet02" {
    vpc_id     = aws_vpc.prj_vpc.id
    availability_zone = var.main_zone02
    cidr_block = "10.10.12.0/24"
    map_public_ip_on_launch = true

    tags = {
        Name = "${var.resrc_prefix}-pub-subnet02"
    }
}

resource "aws_subnet" "app_subnet01" {
    vpc_id     = aws_vpc.prj_vpc.id
    availability_zone = var.main_zone01
    cidr_block = "10.10.21.0/24"
    map_public_ip_on_launch = false

    tags = {
        Name = "${var.resrc_prefix}-app-subnet01"
    }
}

resource "aws_subnet" "app_subnet02" {
    vpc_id     = aws_vpc.prj_vpc.id
    availability_zone = var.main_zone02
    cidr_block = "10.10.22.0/24"
    map_public_ip_on_launch = false

    tags = {
        Name = "${var.resrc_prefix}-app-subnet02"
    }
}

resource "aws_subnet" "db_subnet01" {
    vpc_id     = aws_vpc.prj_vpc.id
    availability_zone = var.main_zone01
    cidr_block = "10.10.31.0/24"
    map_public_ip_on_launch = false

    tags = {
        Name = "${var.resrc_prefix}-db-subnet01"
    }
}

resource "aws_subnet" "db_subnet02" {
    vpc_id     = aws_vpc.prj_vpc.id
    availability_zone = var.main_zone02
    cidr_block = "10.10.32.0/24"
    map_public_ip_on_launch = false

    tags = {
        Name = "${var.resrc_prefix}-db-subnet02"
    }
}

resource "aws_internet_gateway" "prj_igw" {
    vpc_id = aws_vpc.prj_vpc.id

    tags = {
        Name = "${var.resrc_prefix}-igw"
    }
}

resource "aws_route_table" "pub_rt" {
    vpc_id = aws_vpc.prj_vpc.id

    route {
        cidr_block = "0.0.0.0/0"
        gateway_id = aws_internet_gateway.prj_igw.id
    }

    tags = {
        Name = "${var.resrc_prefix}-pub-rt"
    }
}

resource "aws_route_table_association" "pub_assoc_1a" {
    subnet_id      = aws_subnet.pub_subnet01.id
    route_table_id = aws_route_table.pub_rt.id
}

resource "aws_route_table_association" "pub_assoc_1b" {
    subnet_id      = aws_subnet.pub_subnet02.id
    route_table_id = aws_route_table.pub_rt.id
}

resource "aws_route_table" "app_rt" {
    vpc_id = aws_vpc.prj_vpc.id

    route {
        cidr_block  = "0.0.0.0/0"
        instance_id = aws_instance.jumpbox.id
    }

    tags = {
        Name = "${var.resrc_prefix}-app-rt"
    }
}

resource "aws_route_table_association" "app_assoc_1a" {
    subnet_id      = aws_subnet.app_subnet01.id
    route_table_id = aws_route_table.app_rt.id
}

resource "aws_route_table_association" "app_assoc_1b" {
    subnet_id      = aws_subnet.app_subnet02.id
    route_table_id = aws_route_table.app_rt.id
}