resource "aws_security_group" "ssh_sg" {
    name            = "${var.resrc_prefix}-ssh-from-anywhere"
    description     = "Allows SSH accessing from anywhere"
    vpc_id          = aws_vpc.prj_vpc.id

    ingress {
        from_port   = 22
        to_port     = 22
        protocol    = "tcp"
        cidr_blocks = ["0.0.0.0/0"]
        description = "From anywhere"
    }

    egress {
        from_port   = 0
        to_port     = 0
        protocol    = -1
        cidr_blocks = ["0.0.0.0/0"]
        description = "To anywhere"
    }
}

resource "aws_security_group" "webservice_sg" {
    name            = "${var.resrc_prefix}-web-access-from-anywhere"
    description     = "Allows Web accessing from anywhere"
    vpc_id          = aws_vpc.prj_vpc.id

    ingress {
        from_port   = 80
        to_port     = 80
        protocol    = "tcp"
        cidr_blocks = ["0.0.0.0/0"]
        description = "From anywhere"
    }

    ingress {
        from_port   = 443
        to_port     = 443
        protocol    = "tcp"
        cidr_blocks = ["0.0.0.0/0"]
        description = "From anywhere"
    }

    egress {
        from_port   = 0
        to_port     = 0
        protocol    = -1
        cidr_blocks = ["0.0.0.0/0"]
        description = "To anywhere"
    }
}

resource "aws_security_group" "rds_sg" {
    name            = "${var.resrc_prefix}-db-access-from-appsrvs"
    description     = "Allows MySQL(Aurora)DB accessing."
    vpc_id          = aws_vpc.prj_vpc.id

    ingress {
        from_port   = 3306
        to_port     = 3306
        protocol    = "tcp"
        cidr_blocks = ["10.10.21.0/24","10.10.22.0/24","10.10.11.0/24"]
        description = "From AppSrvs & jumpbox"
    }

    egress {
        from_port   = 0
        to_port     = 0
        protocol    = -1
        cidr_blocks = ["0.0.0.0/0"]
        description = "To anywhere"
    }
}

resource "aws_security_group" "cache_sg" {
    name            = "${var.resrc_prefix}-cache-access-from-appsrvs"
    description     = "Allows ElatiCache accessing."
    vpc_id          = aws_vpc.prj_vpc.id

    ingress {
        from_port   = 6379
        to_port     = 6379
        protocol    = "tcp"
        cidr_blocks = ["10.10.21.0/24","10.10.22.0/24","10.10.11.0/24"]
        description = "From AppSrvs & jumpbox"
    }

    egress {
        from_port   = 0
        to_port     = 0
        protocol    = -1
        cidr_blocks = ["0.0.0.0/0"]
        description = "To anywhere & jumpbox"
    }
}

resource "aws_security_group" "mongodb_sg" {
    name            = "${var.resrc_prefix}-mongo-access-from-appsrvs"
    description     = "Allows MongoDB accessing."
    vpc_id          = aws_vpc.prj_vpc.id

    ingress {
        from_port   = 27017
        to_port     = 27017
        protocol    = "tcp"
        cidr_blocks = ["10.10.21.0/24","10.10.22.0/24","10.10.11.0/24"]
        description = "From AppSrvs & jumpbox"
    }

    egress {
        from_port   = 0
        to_port     = 0
        protocol    = -1
        cidr_blocks = ["0.0.0.0/0"]
        description = "To anywhere"
    }
}

resource "aws_security_group" "from_jumpbox_sg" {
    name            = "${var.resrc_prefix}-access-from-jumpbox"
    description     = "Allows accessing from jumpbox."
    vpc_id          = aws_vpc.prj_vpc.id

    ingress {
        from_port   = 22
        to_port     = 22
        protocol    = "tcp"
        cidr_blocks = ["10.10.11.0/24"]
        description = "From jumpbox"
    }

    ingress {
        from_port   = -1
        to_port     = -1
        protocol    = "icmp"
        cidr_blocks = ["10.10.11.0/24"]
        description = "From jumpbox"
    }

    egress {
        from_port   = 0
        to_port     = 0
        protocol    = -1
        cidr_blocks = ["0.0.0.0/0"]
        description = "To anywhere"
    }
}

resource "aws_security_group" "jumpbox_nat_sg" {
    name            = "${var.resrc_prefix}-access-Internet"
    description     = "Allows Internet accessing from AppSrvs."
    vpc_id          = aws_vpc.prj_vpc.id

    ingress {
        from_port   = 80
        to_port     = 80
        protocol    = "tcp"
        cidr_blocks = ["10.10.21.0/24","10.10.22.0/24"]
        description = "From AppSrvs"
    }

    ingress {
        from_port   = 443
        to_port     = 443
        protocol    = "tcp"
        cidr_blocks = ["10.10.21.0/24","10.10.22.0/24"]
        description = "From AppSrvs"
    }

    ingress {
        from_port   = 465
        to_port     = 465
        protocol    = "tcp"
        cidr_blocks = ["10.10.21.0/24","10.10.22.0/24"]
        description = "From AppSrvs"
    }

    ingress {
        from_port   = -1
        to_port     = -1
        protocol    = "icmp"
        cidr_blocks = ["10.10.21.0/24","10.10.22.0/24"]
        description = "From AppSrvs"
    }

    egress {
        from_port   = 0
        to_port     = 0
        protocol    = -1
        cidr_blocks = ["0.0.0.0/0"]
        description = "To anywhere"
    }
}