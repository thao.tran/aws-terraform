#!/bin/bash
cat <<'EOF' >> /etc/profile.d/srvalias.sh
ORIG_USER=$(who am i | awk '{print $1}')
#echo $ORIG_USER
USER=$ORIG_USER
HOST=appsrv00.stg.asgard
SHORT_HOSTNAME=`hostname -s`
#echo "Setting custom prompt..."
PS1="[\u@${HOST} \W]\$ "
export PS1 SHORT_HOSTNAME USER HOST
EOF