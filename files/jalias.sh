ORIG_USER=$(who am i | awk '{print $1}')
#echo $ORIG_USER
USER=$ORIG_USER
HOST=jumpbox.stg.asgard
SHORT_HOSTNAME=`hostname -s`
#echo "Setting custom prompt..."
PS1="[\u@${HOST} \W]\$ "
export PS1 SHORT_HOSTNAME USER HOST